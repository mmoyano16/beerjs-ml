const spoti = require('./spoti.js');
const brain = require('brain.js');
const chalk = require('chalk');

const net = new brain.NeuralNetwork();

// {
//     inputSize: 7,
//     hiddenLayers: [49, 25],
//     outputSize: 2,
//     activation: 'sigmoid', // activation function
//     learningRate: 0.001 
// }

const playlistMigue = '1jQuWwj3zPOLvSGJSz1wVt';
const playlistMigueDislike = '49MGFyEO26E8GiY5vxWMOL';
const playlistPrueba = '0Xx2WdSYNAQuazCJE5W4kb';
const playlistMaza = '6v1rngdmRWBGHRon4W2y5e';
const playlistPruebaTop50Global = '37i9dQZEVXbMMy2roB9myp';

console.error('mensaje rojo');
console.debug('mensaje no se');
console.info('otro mensaje');

spoti.connect().then(
    async () => {
        
        const data = [];

        console.log('... obteniendo datos (like) ...');
        const songs = await spoti.getSongs(playlistMaza);
        
        console.log('... creando el set de entrenamiento (like) ...');
        for (const song of songs) {
            data.push({
                input: song.features,
                output: {like: 1}
            });
        }
        
        console.log('... obteniendo datos (dislike) ...');
        const songsDislike = await spoti.getSongs(playlistMigueDislike);
        
        console.log('... creando el set de entrenamiento (dislike) ...');
        for (const song of songsDislike) {
            data.push({
                input: song.features,
                output: {dislike: 1}
            });
        }

        console.log('... entrenando red ...');
        net.train(data);

        console.log('... obteniendo canciones de prueba ...');
        const testSongs = await spoti.getSongs(playlistPruebaTop50Global);
        
        console.log('... validando ...');
        for (const song of testSongs) {

            const valor = net.run(song.features);
            const nombre = `cancion: ${song.name} - ${song.artist} - like value: ${round(valor.like,6)} - dislike value: ${round(valor.dislike,6)}`;

            if (valor.like <= 0.33) 
                console.log(chalk.red(nombre));
            else if (valor.like <= 0.66) 
                console.log(chalk.yellow(nombre));
            else
                console.log(chalk.green(nombre));
        }
    
    }
);

function round(value, decimals) {
    const n = Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    if (!n) return 0;
    return n;
}